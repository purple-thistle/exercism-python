def response(hey_bob):
    string = hey_bob.strip()
    
    if len(string) == 0:
        return "Fine. Be that way!"

    elif string.isupper(): 
        if string.endswith("?"):
            return "Calm down, I know what I'm doing!"
        return "Whoa, chill out!" 

    elif string.endswith("?"):
        return "Sure."  
        
    else:
        return "Whatever."
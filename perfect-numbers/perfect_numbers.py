def classify(number):

    if number <= 0:
        raise ValueError("Invalid input!")

    res = sum(i for i in range(1,number//2 + 1) if number % i == 0)        

    if number == res:
        return 'perfect'
    elif number < res:
        return 'abundant'
    else:
        return 'deficient'        
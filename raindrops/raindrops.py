sounds={3:"Pling", 5:"Plang", 7: "Plong"}

def convert(number):

    res="".join([sound for i, sound in sounds.items() if number % i == 0])

    return str(res or number)
def value(colors):

    resistors = [
            "black",
            "brown",
            "red",
            "orange",
            "yellow",
            "green",
            "blue",
            "violet",
            "grey",
            "white",
          ]

    return resistors.index(colors[0])*10 + resistors.index(colors[1])
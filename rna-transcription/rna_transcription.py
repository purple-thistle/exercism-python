def to_rna(dna_strand):
    subs = {'G': 'C', 'C': 'G', 'T': 'A', 'A': 'U'}

    return "".join([subs[i] for i in dna_strand if i in subs])   
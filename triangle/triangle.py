def equilateral(sides):
    return inequality(sides) and len(set(sides)) == 1


def isosceles(sides):
    return inequality(sides) and len(set(sides)) < 3


def scalene(sides):
    return inequality(sides) and len(set(sides)) == 3


def inequality(sides):
    a, b, c = sorted(sides)

    return a * b * c != 0 and a + b >= c